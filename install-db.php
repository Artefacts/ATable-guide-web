<?php
require_once "wakka.config.php";
// Name of the file
// MySQL host
$mysql_host = $wakkaConfig['mysql_host'];
// MySQL username
$mysql_username = $wakkaConfig['mysql_user'];
// MySQL password
$mysql_password = $wakkaConfig['mysql_password'];
// Database name
$mysql_database = $wakkaConfig['mysql_database'];
$mysql_table_prefix = $wakkaConfig['table_prefix'];


$dsn = 'mysql:dbname='.$mysql_database.';host='.$mysql_host;
try {
    
    $db = new PDO($dsn, $mysql_username, $mysql_password);
	$lines = file_get_contents('__GuideATABLEVierge.sql') ;
	
	$lines_t = str_replace('ATABLE_', $mysql_table_prefix, $lines);
	$qr = $db->exec($lines_t);
	echo '<h2>Ajout des tables réussi !</h2>' ;
	} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}
	echo '<a href="/"> << Retour au site</a>' ;