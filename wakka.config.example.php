<?php
// wakka.config.php cr&eacute;&eacute;e Tue Jan 30 15:20:15 2018
// ne changez pas la yeswiki_version manuellement !
//CRLF new lines
$wakkaConfig = array (
  'wakka_version' => '0.1.1',
  'wikini_version' => '0.5.0',
  'yeswiki_version' => 'cercopitheque',
  'yeswiki_release' => '2017-11-25-1',
  'debug' => 'no',
  'mysql_host' => 'localhost',
  'mysql_database' => 'databasename',
  'mysql_user' => 'user',
  'mysql_password' => 'password',
  'table_prefix' => 'ATABLE_',
  'base_url' => 'http://localhost/?',
  'rewrite_mode' => '0',
  'meta_keywords' => 'alimentation, biologique, locale, équitable',
  'meta_description' => 'Le guide de l\' Association Tourangelle pour une Alimentation Biologique Locale Equitable',
  'action_path' => 'actions',
  'handler_path' => 'handlers',
  'header_action' => 'header',
  'footer_action' => 'footer',
  'navigation_links' => 'DerniersChangements :: DerniersCommentaires :: ParametresUtilisateur',
  'referrers_purge_time' => 24,
  'pages_purge_time' => 90,
  'default_write_acl' => '*',
  'default_read_acl' => '*',
  'default_comment_acl' => '@admins',
  'preview_before_save' => 0,
  'allow_raw_html' => '1',
  'timezone' => 'GMT',
    'root_page' => 'PagePrincipale',
    'wakka_name' => 'Atable',
    'default_language' => 'fr',
    'favorite_theme' => 'bootstrap3',
    'favorite_squelette' => '1col-atable.tpl.html',
    'favorite_style' => 'bootstrap.min.css',
	
	/*Personnalisations map page carte*/
	'baz_map_zoom' => 9, // Niveau de zoom pour les petits écrans (<768px)
	//Centrage de la carte"
	'baz_map_center_lat' => 47.3936,
	'baz_map_center_lon' => 0.6892,
	// Si utilisateur géolocalisé
	'baz_geo_disttance_max' => 130, // Nombre de Kilomètres à partir du centre de la carte au delà desquels le message aucun résultats apparaîtra pour la recherche
	'baz_map_zoom_if_geo' => 12, // zoom si geolocalisé
  
  
	/*Envoi de mails */
	'BAZ_ENVOI_MAIL_ADMIN' => true,
  'BAZ_ADRESSE_MAIL_ADMIN' => 'email@test.com',
  'BAZ_ADRESSE_MAIL_GROUPE' => 'gestion',
);

require_once "devfiles/lessc.inc.php";
$less = new lessc;
$less->checkedCompile("personnalisations/styles.less.css", "themes/bootstrap3/styles/styles.css");

/*Labels personnalisés */
require_once "personnalisations/labels/labels-personnalises.php";
