<?php

/*****************************************************************************/
/*       Fichier de personnalisation des textes affichées sur le site        */
/*****************************************************************************/
/*
COMMENT ÇA MARCHE ?
	Après la ligne "$LABELS_SITE = array(", on trouve deux types de lignes.
	Les lignes débutant par "//" sont des lignes de commentaires. Elles n'ont pour but que de vous informer.
	Les autres lignes sont de la forme ci-dessous.
	"UnTexteEntreGuillemets" => 'un texte entre quotes',
	Le texte entre guillemets ne doit JAMAIS être modifié. Il s'agit d'une "clé" qui permet au système de trouver ce qu'il cherche.
	Le texte entre '' (situé après "=>" et avant la virgule) est celui qui s'affichera à l'écran.
	Autrement dit
	"NeJamaisToucherAÇa" => 'Ceci est ce que vous devez modifier' ,
CE QU'IL FAUT FAIRE
	1. Chercher la ligne correspondant au texte que vous souhaitez changer.
	2. Modifier le texte entre '' de façon à ce qu'il vous convienne (ATTENTION : laissez bien les '' en place).
	3. Enregistrer ce fichier une fois que vous avez terminé.
*/

global $LABELS_SITE ;

$LABELS_SITE = array(
//FORMULAIRE DE RECHERCHE
	//libellé affiché dans le champ de sélection des produits sur la page de recherche
	"FormSearch_AideChampProduits" => 'Sélectionner un produit',
	//libellé affiché dans le champ de sélection des modes de vente sur la page de recherche
	"FormSearch_AideChampModeVente" => 'Sélectionner un mode de vente',
	//libellé affiché dans le champ de saisie pour la recherche textuelle sur la page de recherche
	"FormSearch_AideChampMotsCles" => 'Mots clés',
	//libellé affiché sur le bouton qui lance la recherche sur la page de recherche
	"FormSearch_BoutonRecheche" => 'Lancer la recherche',
	//libellé affiché suite à une recherche infructueuse
	"FormSearch_MessNoResult" => 'Pas de réponse pour <br class="visible-xs" />ces critères de recherche.',
	//libellé affiché sur le bouton qui affiche la carte suite à une recherche infructueuse
	"FormSearch_BoutonNoResult" => 'Afficher quand même la carte',

//AFFICHAGE D'UNE FICHE
	//libellé affiché devant la latitude lors de la visualisation d'une fiche
	"Fiche_LabelLatitude" => "Lat : ",
	//libellé affiché devant la longitude lors de la visualisation d'une fiche
	"Fiche_LabelLongitude" => " Lon : ",

	//libellé affiché sur le bouton de partage en bas de la visualisation d'une fiche
	"Fiche_BoutonPartageTexte" => "Partager",
	//libellé affiché sur le bouton de modification de la fiche en bas de la visualisation d'une fiche
	"Fiche_BoutonEditionTexte" => "Modifier",
	//libellé affiché sur le bouton de demande suppression en bas de la visualisation d'une fiche
	"Fiche_BoutonSuppressionTexte" => "Supprimer",
	//libellé affiché sur le bouton de retour à la carte en bas de la visualisation d'une fiche
	"Fiche_BoutoRetourTexte" => "Retour",

	//libellé affiché avant les options de vente lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeVntSurPlaceLabel" => ' <span class="color-violet"><strong>Vente</strong></span> ',
	//libellé affiché en cas de vente sur place lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeVntSurPlaceOui" => '&nbsp;<span class="color-violet">&#9679;</span> Sur place' ,
	//libellé affiché s'il n'y a pas de vente sur place lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeVntSurPlaceNon" => '&nbsp;<span class="color-violet">&#9679;</span> Pas de vente sur place' ,
	//texte affiché avant les horaires de vente lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeVntSurPlaceAvantHoraire" => "&nbsp;: " ,
	//Libellé affiché en cas de vente en AMAP lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntAMAPLabel" => '&nbsp;<span class="color-violet">&#9679;</span> AMAP',
	//texte affiché avant les détails de vente en AMAP lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntAMAPLabelCMt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente en groupement de consommateurs lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntGrpCmmLabel" => '&nbsp;<span class="color-violet">&#9679;</span> Groupement de consommateurs',
	//texte affiché avant les détails de vente en groupement de consommateurs lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntGrpCmmLabelCmt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente au sein d'une "ruche qui dit oui" lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntRucheLabel" => '&nbsp;<span class="color-violet">&#9679;</span> Ruche qui dit oui',
	//texte affiché avant les détails de vente au sein d'une "ruche qui dit oui" lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntRucheLabelCmt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente sur les marchés lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntMarcheLabel" => '&nbsp;<span class="color-violet">&#9679;</span> Marché(s)',
	//texte affiché avant les détails de vente sur les marchés lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntMarcheLabelCmt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente en magasin lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntmagasinsLabel" => '&nbsp;<span class="color-violet">&#9679;</span> Magasin(s)' ,
	//texte affiché avant les détails de vente en magasin lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntmagasinsLabelCmt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente à un restaurant lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntRestaurantsLabel" => '&nbsp;<span class="color-violet">&#9679;</span> Restaurant(s)',
	//texte affiché avant les détails de vente à un restaurant lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntRestaurantsLabelCmt" => "&nbsp;: " ,
	//Libellé affiché en cas de vente en AMAP lors de la visualisation de la fiche d'un producteur
	"Fiche_ListeListeVntAutreLabel" => '&nbsp;<span class="color-violet">&#9679;</span> ',
	
	
	//Titre de la boite de dialogue pour partager une fiche
	"Fiche_TitrePartage" => 'Partager ce lien avec d\'autres',
	//libellé affiché sur le bouton servant à copier l'url de la boite de dialogue pour partager une fiche
	"Fiche_BoutonCopierPartage" => 'Copier',
	//libellé affiché sur le bouton servant à fermer la boite de dialogue pour partager une fiche
	"Fiche_BoutonFermerPartage" => 'Fermer',

//CONTRIBUTION
	//Messages affichés à l'utilisateur suite à une contribution
	//Message si demande d'ajout.
	"AjoutFiche_MessDemande" => "Merci, votre fiche apparaîtra sur la carte après validation par l'équipe d'ATABLE.",
	//Utilisé pour une modification et non un ajout
	"FormFiche_MessAttenteValidation" => "Merci, vos modifications sur la fiche seront visibles sur la carte après validation par l'équipe d'ATABLE",
	//Message si demande de suppression.
	"SuppFiche_MessDemande" => "Merci pour votre demande de suppression. Nous allons contacter la structure concernée et traiter votre demande.",
	//Message si abandon de suppression.
	"SuppFiche_MessAnnule" => "Cette fiche ne sera pas supprimée.",

	//Objet des mails envoyés à l'équipe de gestion suite à une contribution
	"ObjMail_AjoutFiche" => "Creation demandee",
	"ObjMail_ModifFiche" => "Modification demandee",
	"ObjMail_SuppFicheDemande" => "Suppression demandee",

	//Objet des mails envoyés à l'équipe de gestion suite à une intervention de gestion
	"ObjMail_ValidFiche" => "Fiche validee",
	"ObjMail_SuppFiche" => "Fiche supprimee",

//SAISIE D'UNE FICHE
	//Message affiché si un champ obligatoire n'a pas été rempli lors de l'édition d'une fiche.
	"FormFiche_MessChampOblVide" => "Un champ obligatoire n'est pas renseigné!",
	//Message affiché si le bouton de localisation de l'adresse n'a pas été activé lors de la saisie de l'adresse sur une fiche.
	"FormFiche_MessChampGeoloc" => " Veuillez cliquer sur ce bouton pour positionner l'adresse sur la carte.",
);