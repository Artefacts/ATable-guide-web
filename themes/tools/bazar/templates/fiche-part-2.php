<?php
global $LABELS_SITE ;
?>
	
	<div class="row">
		<div class="col-xs-12 text-center sec-boutons-fiche">
		
		<?php
$url_site = $GLOBALS['wiki']->GetConfigValue('base_url') ;

$sujet_mail = ''.$fiche['bf_titre'].' sur le guide Atable' ;
$body_mail = $url_site.$fiche['id_fiche'] ;

$bouton_mail = '<a class="btn-partage" data-fiche-title="'.$sujet_mail.'" data-link="'.$body_mail.'" href="#">'.$LABELS_SITE['Fiche_BoutonPartageTexte'].'</a>' ;


if( isset($fiche['id_fiche']) )
{
    $bouton_edit = '<a class="btn-modifier" href="'.$url_site.$fiche['id_fiche'].'/edit">'.$LABELS_SITE['Fiche_BoutonEditionTexte'].'</a>' ;
    $bouton_supprimer = '<a class="btn-supprimer btn-suppr-confirm" href="'.$url_site.$fiche['id_fiche'].'/supprimer" rel="nofollow">'.$LABELS_SITE['Fiche_BoutonSuppressionTexte'].'</a>' ;
}
else
{
    $bouton_edit = '' ;
    $bouton_supprimer = '' ;
}
$bouton_fermer = '<a class="btn-close-full-fiche" href="#">'.$LABELS_SITE['Fiche_BoutoRetourTexte'].'</a>' ;

		echo $bouton_mail.$bouton_edit.$bouton_supprimer.$bouton_fermer ;
		?>

		</div>
	</div>
</div>

<div class="yeswiki-footer-2">
    <div class="container">
     <?php echo $GLOBALS['wiki']->Format(
    '{{include page="PageFooter2" doubleclic="1"}}' ) ; ?>
	</div>
</div>
<div id="partager" style="display:none;">
	<div class="partage-overlay"></div>
	<div class="partage-wrap">
		<h3><?php echo $LABELS_SITE['Fiche_TitrePartage'] ; ?></h3>
		<div class="wrap-url"><span id="partage-url-input" v><?php echo $body_mail ; ?></span><a href="#"><?php echo $LABELS_SITE['Fiche_BoutonCopierPartage'] ; ?></a></div>
		<div class="wrap-close"><a href="#"><?php echo $LABELS_SITE['Fiche_BoutonFermerPartage'] ; ?></a></div>
	</div>
</div>
