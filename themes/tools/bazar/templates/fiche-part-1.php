<?php
global $LABELS_SITE ;

$valeursListeTypestructure = baz_valeurs_liste('ListeTypestructure');
$valeursListeCertification = baz_valeurs_liste('ListeCertification');
$valeursListeProduits = baz_valeurs_liste('ListeProduits');
$valeursListeVntamap = baz_valeurs_liste('ListeVntamap');
$valeursListeVntgrpcmm = baz_valeurs_liste('ListeVntgrpcmm');
$valeursListeVntmagasins = baz_valeurs_liste('ListeVntmagasins');
$valeursListeVntmarche = baz_valeurs_liste('ListeVntmarche');
$valeursListeVntrestaurants = baz_valeurs_liste('ListeVntrestaurants');
$valeursListeVntruche = baz_valeurs_liste('ListeVntruche');
$valeursListeVntsurplace = baz_valeurs_liste('ListeVntsurplace');

$array_champs_prod_infos_sup = array (
1 => 'bf_Legumes',
2 => 'bf_Panier',
3 => 'bf_Fruits',
4 => 'bf_Champignons',
5 => 'bf_LegumesSecs',
6 => 'bf_Cereales',
7 => 'bf_Huile',
8 => 'bf_Pain',
9 => 'bf_Jus',
10 => 'bf_Miel',
11 => 'bf_OEufs',
12 => 'bf_Lait',
13 => 'bf_Patisseries',
14 => 'bf_Volailles',
15 => 'bf_Viandes',
16 => 'bf_Biere',
17 => 'bf_Vin',
);


$is_admin = false ;
	$save = true ;
	foreach ($GLOBALS['wiki']-> GetGroupsList() as $key => $val) {
		if ($GLOBALS['wiki']->UserIsInGroup($val) ) {
			$is_admin = true;
		} 
    }
	
	//var_dump($fiche) ;
if ($fiche['radioListeSuppression'] == '2' AND $is_admin === false) {
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
	include("404.html");
    exit;
}



$temp = 'imagebf_image1' ;
if (!empty($fiche[$temp])) {

	$class_block_titre = 'col-xs-8 col-md-9' ;
} else {
	 $class_block_titre = 'col-xs-12' ;
}
if (isset($_GET['message_att'])) {
	$message_get = $_GET['message_att'] ;
} else {
	$message_get = false ;
}

if (isset($_POST['deleteexit'])) {
	$message_get = $LABELS_SITE['SuppFiche_MessAnnule'] ;
}
	

?>
<div id="content-fiches-1-2-3-4-5" class="container">
<?php if ($message_get) { ?>
	<div class="alert alert-success" role="alert"><?php echo $message_get ; ?></div>

<?php } ?>
<div class="row col-intro">
	<div class="<?php echo $class_block_titre ;?>">
<?php
$temp = 'listeListeTypeStructure' ;
if (!empty($fiche[$temp])) {

	$key = $fiche[$temp] ;
	//echo $key ;
	echo '<p><span class="label opt-'.$key.'">'.$valeursListeTypestructure['label'][$key].'</span></p>' ;

}





$temp = 'bf_titre' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<h2 class="'.$temp.'"><span>'.$fiche[$temp].'</span></h2>' ;

}



$temp = 'checkboxListeProduits' ;
if (!empty($fiche[$temp]))
{
    $temp_array = explode (',', $fiche[$temp]) ;
	echo '<p class="'.$temp.'">' ;
	foreach ($temp_array as $value)
	{
		$key = $value ;
		//echo $value ;
		if( isset($valeursListeProduits['label'][$key]) )
		{
			echo '<span class="opt-'.$key.'" data-toggle="tooltip" data-placement="bottom" title="'
	            . $valeursListeProduits['label'][$key]
	            .'"></span>' ;
		}
	}
	echo '</p>' ;
}
?>
	</div>
	<div class="col-xs-4 col-md-3">
<?php
$temp = 'imagebf_image1' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<p class="'.$temp.'" style="background-image: url(files/'.$fiche[$temp].');"></p>' ;

}

?>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-xs-12">
<?php
$echo_hr = false ;
$temp = 'bf_telGuide' ;
if (!empty($fiche[$temp])) {
	$data = $fiche[$temp];

	if(  preg_match( '/^(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/', $data,  $matches ) )
	{
		$result = $matches[1] . ' ' .$matches[2] . ' ' . $matches[3] . ' ' . $matches[4]. ' ' . $matches[5];
	} else {
		$result = $fiche[$temp];
	}
	//echo $fiche[$temp] ;
	echo '<p class="icon-pad '.$temp.'"><span><a href="tel:'.$fiche[$temp].'">'.$result.'</a></span></p>' ;
	$echo_hr = true ;
}

$temp = 'bf_mailGuide' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<p class="icon-pad '.$temp.'"><a href="mailto:'.$fiche[$temp].'" ><span>'.$fiche[$temp].'</span></a></p>' ;
	$echo_hr = true ;
}
$temp = 'bf_site_internet' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<p class="icon-pad '.$temp.'"><a href="'.$fiche[$temp].'" ><span>'.$fiche[$temp].'</span></a></p>' ;
	$echo_hr = true ;
}
if ($echo_hr) {
	echo '<hr/>' ;
}


$echo_hr = false ;
$temp = 'bf_NomContact' ;
$tempb = 'bf_PrenomContact' ;
if ((!empty($fiche[$temp])) or (!empty($fiche[$tempb])) ) {

	echo '<p class="icon-pad '.$temp.$tempb.'">' ;
	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$temp.'">'.$fiche[$temp].' </span>' ;
		$echo_hr = true ;
	}


	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$tempb.'">'.$fiche[$tempb].'</span>' ;
		$echo_hr = true ;
	}
	echo '</p>' ;
}
if ($echo_hr) {
	echo '<hr/>' ;
}

$echo_hr = false ;
$temp = 'bf_adresse1' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<p class="icon-pad '.$temp.'"><span>'.$fiche[$temp].'</span></p>' ;
	$echo_hr = true ;
}

$temp = 'bf_code_postal' ;
$tempb = 'bf_ville' ;
if ((!empty($fiche[$temp])) or (!empty($fiche[$tempb])) ) {

	echo '<p class="icon-pad '.$temp.$tempb.'">' ;
	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$temp.'">'.$fiche[$temp].' </span>' ;

	}


	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$tempb.'">'.$fiche[$tempb].'</span>' ;

	}
	echo '</p>' ;
	$echo_hr = true ;
}
if ($echo_hr) {
	echo '<hr/>' ;
}


$temp = 'bf_latitude' ;
$tempb = 'bf_longitude' ;
if ((!empty($fiche[$temp])) or (!empty($fiche[$tempb])) ) {

	echo '<p class="icon-pad '.$temp.$tempb.'">' ;
	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$temp.'">'.$LABELS_SITE['Fiche_LabelLatitude'].$fiche[$temp].'</span><br/>' ;

	}


	if (!empty($fiche[$temp])) {

		//echo $fiche[$temp] ;
		echo '<span class="'.$tempb.'">'.$LABELS_SITE['Fiche_LabelLongitude'].$fiche[$tempb].'</span>' ;

	}
	echo '</p>' ;
}

?>
	</div>
</div>


<hr/>
<div class="row">

<?php
$echo_hr = false ;
$images = array() ;
$temp = 'imagebf_image1' ;
if (!empty($fiche[$temp])) {
	//echo $fiche[$temp] ;
	$images[] = $fiche[$temp] ;

}
$temp = 'imagebf_image2' ;
if (!empty($fiche[$temp])) {
	//echo $fiche[$temp] ;
	$images[] = $fiche[$temp] ;

}

$temp = 'imagebf_image3' ;
if (!empty($fiche[$temp])) {
	//echo $fiche[$temp] ;
	$images[] = $fiche[$temp] ;

}
if (sizeof($images) > 0) {
$li = '' ;$items =''; $i = 0 ;
foreach ($images as $value) {
$active = '';
if ($i == 0 ) {
	$active = 'active' ;
} else {
	$active ='not-active' ;
}
 $li .='<li data-target="#carousel-generic" data-slide-to="'.$i.'" class="'.$active.'"></li>' ;
 $items .= '<div class="item '.$active.'">
			  <img class="img-responsive" src="files/'.$value.'" />
			</div>' ;

	$i++ ;
}


?>

	<div class="col-xs-12 col-md-12">
		<div id="carousel-generic" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		   <?php echo $li ; ?>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
			<?php echo $items ; ?>
		  </div>

		  
		</div>
	</div>


<?php
$echo_hr = true ;
} // end sizeof $images > 0
?>


<?php

$temp = 'bf_Chapo' ;
if (!empty($fiche[$temp])) {

	//echo $fiche[$temp] ;
	echo '<div class="col-xs-12 col-md-12"><p class="'.$temp.'"><span>'.$fiche[$temp].'</span></p></div>' ;
	$echo_hr = true ;
}





?>

</div>
<?php
if ($echo_hr) {
	echo '<hr/>' ;
}
?>
<div class="row">
	<div class="col-xs-12">
<?php



$temp = 'checkboxListeProduits' ;
if (!empty($fiche[$temp])) {

echo '<div class="sec-'.$temp.'">' ;
$temp_array = explode (',', $fiche[$temp]) ;
	//echo $fiche[$temp] ;
	$i = 0 ;
	$ib = 0 ;
	$first_list ='' ;
	$second_list ='' ;
	foreach ($temp_array as $value)
	{

		$key = $value ;
		//echo $value ;
		$key_complement = isset($array_champs_prod_infos_sup[$key]) ? $array_champs_prod_infos_sup[$key] : null ;

		if( !empty($key_complement) && !empty($fiche[$key_complement]) ){

		$first_list .= '<div class="col-xs-12 col-md-6"><span class="opt-'.$key.'">'
			.$valeursListeProduits['label'][$key].' : '.$fiche[$key_complement].'</span></div>' ;

			$ib++ ;
			if ($ib % 2 == 0) {
				$first_list .= ' <div class="clearfix hidden-xs"></div>' ;
			}

		} else {

			if( isset($valeursListeProduits['label'][$key]) )
			{
					//error_log(__FILE__.' key:['.$key.']');
				$second_list .= '<div class="col-xs-12 col-md-6">'
									.'<span class="opt-'.$key.'">'
									.$valeursListeProduits['label'][$key]
									.'</span></div>' ;

				$i++ ;

				if ($i % 2 == 0) {
					$second_list .= ' <div class="clearfix visible-md-block visible-lg-block"></div>' ;
				}

			}
		}


	}
	echo $first_list.' <div class="clearfix"></div>'. $second_list .' <div class="clearfix"></div>';

	if ($fiche['bf_autresProduits'] != '') {

		echo '<div class="col-xs-6 col-sm-4 col-md-3"><span class="opt-bf_autresProduits">'.$fiche['bf_autresProduits'].'</span></div>' ;

	}

echo '</div> <div class="clearfix"></div>' ;


}
?>

<?php

$temp = 'radioListeCertification' ;
if (!empty($fiche[$temp])) {
echo '<hr/>' ;
	$key = $fiche[$temp] ;
	//echo $key ;
	echo '<p class="icon-pad '.$temp.'"><span class="'.$key.'">'.$valeursListeCertification['label'][$key].'</span></p>' ;

}
/*
$temp = 'listeListeVntSurPlace' ;
if (!empty($fiche[$temp])) {
echo '<hr/>' ;
	$key = $fiche[$temp] ;
	//echo $key ;
	echo '<p class="icon-pad '.$temp.'"><span class="'.$key.'">'.$valeursListeVntsurplace['label'][$key].'</span></p>' ;

}

$temp = 'bf_Horaires' ;
if (!empty($fiche[$temp])) {
echo '<hr/>' ;
	//echo $fiche[$temp] ;
	echo '<p class="icon-pad '.$temp.'"><span class="'.$key.'">'.$fiche[$temp].'</span></p>' ;
}
*/
//var_dump($fiche) ;
?>

	</div>
</div>

<?php
