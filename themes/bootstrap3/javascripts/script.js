$(document).ready(function() {


	
	$('#formulaire').each(function() {
        //var str_page_ajout = 'PageAjout' ;
        //console.log($('#bf_mode_formulaire').val()) ;
        if ($('#bf_mode_formulaire').val() == 'saisie') {
            //if (location.href.match(str_page_ajout) != null) {
            $(this).addClass('formulaire-ajout');
        } else {
            $(this).addClass('formulaire-modification');


            /*if ($('body').hasClass('in_admin_group')) {

                if ($('#bf_mode_revision').val() == 'oui') {

                    $(this).append('<input type="hidden" name="bf_modifications_validee" id="bf_modifications_validee" value="0" />');
                    $(this).find('.tab-pane').last().append('<button class="pull-right btn btn-lg btn-success bouton_sauver" id="publier_version" name="publier">Valider les changements</button>');

                    $('#publier_version').bind('click', function() {
                        $('#bf_modifications_validee').val(1);
                        $('#bf_modifications_validee').parent('form').submit();
                    });
                }
            }*/
        }
        $('#bf_adresse1Perso, #bf_code_postalPerso, #bf_villePerso, #bf_paysPerso, #bf_telFixePerso, #bf_telCellPerso, #bf_mailPerso, #menu2 h3').each(function() {
            $(this).parents('.hidden-intput').addClass('visible-on-add-for-all');
        });
        if ($('#bf_adresse1Perso').parents('div.hidden-intput').length > 0) {
            $('#menu2 h3').addClass('hidden-intput');
            $('#menu2 h3').addClass('visible-on-add-for-all');
        }

    })

    $('ul.navbar-nav>li:first-child a').prepend('<span class="searh-menu-item"></span>');

	
	trigger_event_suppress() ;

    jQuery('.nav-tabs a, .tab-pane>a').click(function(e) {

        var tabRef = $(e.currentTarget).attr('href');
        if (tabRef) {
            // Refresh map on tab display
            if ($('#osmmapform', $(tabRef)))
                L.Util.requestAnimFrame(map.invalidateSize, map, !1, map._container);
        }

        e.preventDefault();
    });

    $('#formulaire #bf_latitude').parent('div').find('a').on('click', function() {
        setTimeout(function() { check_all_inputs_form(); }, 500);
    });
    $('#formulaire .btn-geolocate-address').each(function() {
        var form_group = $(this).parents('div.form-group')[0];
        $(form_group).find('label').html('<span class="symbole_obligatoire">*&nbsp;</span>');
        var text = $('div[data-label-key="FormFiche_MessChampGeoloc"]').text();
        $(this).after('<span>' + text + '</span>');
    });

    $('#formulaire .tab-pane').each(function() {
        var text = $('div[data-label-key="FormFiche_MessChampOblVide"]').text();
        $(this).find('.control-group').last().after('<div id="' + $(this).attr('id') + '_message" class="text-right text-danger" style="display:none;"><strong>' + text + '</strong></div>');
    });

    $('#formulaire input[required="required"], #formulaire select[required="required"], #formulaire textarea[required="required"], #formulaire .chk_required input:checkbox, #formulaire #bf_latitude, #formulaire #bf_longitude').each(function() {
        $(this).bind('change', function() {
            check_all_inputs_form();
        });
    });

    function check_all_inputs_form() {

        var at_all_inputs_done = true;
        var this_form = $('#formulaire');


        $('#formulaire .tab-pane').each(function() {
            var this_pane_all_ok = true;
            var pan_id_message = $(this).attr('id') + '_message';
            $('#' + pan_id_message).hide();
            $(this).find('input[required="required"], select[required="required"], textarea[required="required"], .chk_required input:checkbox, #bf_latitude, #bf_longitude').each(function() {
                var val = return_val_input_form(this);

                var this_parent_display = $(this).parents('div[id]:not(.tab-pane)')[0];
                //console.log(this_parent_display) ;

                if (this_parent_display != undefined) {
                    var id_parent = $(this_parent_display).attr('id');
                    var dependant_de = id_parent.split("_");
                    var val_dep = $('#' + dependant_de[0]).val();
                    if (id_parent == dependant_de[0] + '_' + val_dep) { //doit ^tre remplie
                        val = $(this).val();

                    } else { //peut être vide
                        val = 'ok';
                    }
                }

                var tabs_form = $(this).parents('.tab-pane');
                if (val == '') {
                    this_pane_all_ok = false;
                }
            });

            if (this_pane_all_ok == false) {
                $(this).find('a.pull-right[role="tab"]').addClass('disabled');
                at_all_inputs_done = false;

                $('#' + pan_id_message).show();


            } else {
                $(this).find('a.pull-right[role="tab"]').removeClass('disabled');
            }
        });
        if (at_all_inputs_done == true) {
            $(this_form).find('button.pull-right[type="submit"]').removeAttr('disabled', '');
        } else {
            $(this_form).find('button.pull-right[type="submit"]').attr('disabled', 'disabled');
        }
    }
    check_all_inputs_form();

    function return_val_input_form(this_) {
        var this_parent_display = $(this_).parents('div[id]:not(.tab-pane)')[0];
        var this_display = $(this_parent_display).css('display');
        //console.log(this_display) ;
        if (this_display == 'none') {
            //console.log(this_) ;
            return true;
        }
        if ($(this_).attr('type') == 'checkbox') {
            var val = '';
            if ($(this_).parents('.chk_required').find('input:checked').length > 0) {
                val = true;
            }
        } else {
            var val = $(this_).val();
        }
        return val;
    }


    if (navigator.geolocation) {
        // L'API est disponible
        /*var lat = Cookies.get('lat');
        var lon = Cookies.get('lon');*/
        var lat = parseFloat($("#geoloc-lat").val());
        var lon = parseFloat($("#geoloc-lon").val());
        var geo_zoom = $("#geoloc-zoom").val();
        var geo_zoom_default = $("#geoloc-zoom-default").val();

        $('.get-geo').bind('click', function() {
            $('.button-geo-loc-wrap button').addClass('run');
            navigator.geolocation.getCurrentPosition(quick_position, erreur_position, { maximumAge: 600000 });

            return false;
        });

        $('.geo-button').bind('click', function() {
            //lat = parseFloat($("#geoloc-lat").val());
            //lon = parseFloat($("#geoloc-lon").val());
            /*console.log(lat);
            console.log(lon);*/
           /* if (lat != 'false' && lon != 'false') {
				if (geo_zoom != 'default') {
					map1.steView([lat, lon], geo_zoom);
				} else {
					map1.setView(new L.LatLng(lat, lon), geo_zoom_default);
				}
                
            } else {*/
                $('.button-geo-loc-wrap button').addClass('run');
                navigator.geolocation.getCurrentPosition(quick_position, erreur_position, { maximumAge: 600000 });
                /*lat = parseInt($("#geoloc-lat").val());
                lon = parseInt($("#geoloc-lon").val());*/
                /*if (lat != 0 && lon != 0) {
                    map1.panTo(new L.LatLng(lat, lon));
                }*/
            //}


            return false;
        });

        $('#no-ressult-warning a.close-message').bind('click', function() {
            $('#no-ressult-warning').hide();

            return false;
        });

        if (lat != 'false' && lon != 'false') {
            /*console.log(lat);
            console.log(lon);
            console.log('geo-on');*/
            if ($('#search-carte-results').length > 0) {
	//alert(geo_zoom) ;
               if (geo_zoom != 'default') {
					map1.setView([lat, lon], geo_zoom);
				} else {
					map1.setView(new L.LatLng(lat, lon), geo_zoom_default);
				}
            }
        } else {
            //console.log('no-geo-search');
        }


    } else {
        // Pas de support, proposer une alternative ?
        $('.get-geo').hide();
        //console.log('no-geo');
    }
	/*var set_view_options = {
		 
		  "pan": {
			"animate": true,
			"duration": 10
		  },
		  "zoom": {
			"animate": true
		  }
	} ;*/

    function quick_position(position) {
        var infopos = "Position déterminée :\n";
        infopos += "Latitude : " + position.coords.latitude + "\n";
        infopos += "Longitude: " + position.coords.longitude + "\n";
        infopos += "Altitude : " + position.coords.altitude + "\n";
        $("#geoloc-lat").attr("value", position.coords.latitude);
        $("#geoloc-lon").attr("value", position.coords.longitude);
        // document.getElementById("infoposition").innerHTML = infopos;
        console.log(infopos);
        /*Cookies.set('lat', position.coords.latitude, { expires: 0.5 });
        Cookies.set('lon', position.coords.longitude, { expires: 0.5 });*/
        $('.button-geo-loc-wrap button').removeClass('run');
        $('.error-geoloc').html('');

        if ($('#search-carte-results').length > 0) {

			var geo_zoom = $("#geoloc-zoom-located").val();
			//alert(geo_zoom) ;
            map1.setView(new L.LatLng(position.coords.latitude, position.coords.longitude), geo_zoom);
			
        }

    }

    function user_position(position) {
        var infopos = "Position déterminée :\n";
        infopos += "Latitude : " + position.coords.latitude + "\n";
        infopos += "Longitude: " + position.coords.longitude + "\n";
        infopos += "Altitude : " + position.coords.altitude + "\n";
        // document.getElementById("infoposition").innerHTML = infopos;
        //console.log( infopos) ;
        if ($('.js-data-ville').find("#geo-auto").length) {
            $('.js-data-ville').find("#geo-auto").attr("value", position.coords.latitude + '#' + position.coords.longitude);
            $('.js-data-ville').val(position.coords.latitude + '#' + position.coords.longitude).trigger('change');
        } else {
            // Create a DOM Option and pre-select by default
            var newOption = '<option value="' + position.coords.latitude + '#' + position.coords.longitude + '" id="geo-auto">Ma position</option>';
            $('.js-data-ville').prepend(newOption).trigger('change');
        }
        $('.js-data-ville').val(position.coords.latitude + '#' + position.coords.longitude); // Select the option with a value of '1'
        $('.js-data-ville').trigger('change'); // Notify any JS components that the value changed
        $('.button-geo-loc-wrap button').removeClass('run');
        $('.error-geoloc').html('');
    }

    function erreur_position(error) {
        var info = "Erreur lors de la géolocalisation : ";
        switch (error.code) {
            case error.TIMEOUT:
                info += "Timeout !";
                break;
            case error.PERMISSION_DENIED:
                info += "Vous n’avez pas donné la permission";
                break;
            case error.POSITION_UNAVAILABLE:
                info += "La position n’a pu être déterminée";
                break;
            case error.UNKNOWN_ERROR:
                info += "Erreur inconnue";
                break;
        }
        /* console.log(info);*/
        $('.button-geo-loc-wrap button').removeClass('run');
        $('.error-geoloc').html(info);
		
		if ($('#search-carte-results').length > 0) {
			var lat = parseFloat($("#geoloc-lat").val());
            var lon = parseFloat($("#geoloc-lon").val());
			var geo_zoom_default = $("#geoloc-zoom-default").val();
            map1.setView(new L.LatLng(lat, lon), geo_zoom_default);
        }
		
		return false;

    }

    function formatState(state) {

        if (!state.id) {
            return state.text;
        }


        var $state = $(
            '<span class="opt-' + state.id + '">' + state.text + '</span>'
        );
        return $state;
    };

    function formatStateSel(state) {

        if (!state.id) {
            return state.text;
        }


        var $state = $(
            '<span class="sel-format opt-' + state.id + '">' + state.text + '</span>'
        );
        return $state;
    };

    $('.select2-1-b').select2({
        width: '100%',
        placeholder: $('.select2-1-b').attr('placeholder'),
        closeOnSelect: false,
        tags: true,
        templateResult: formatState,
        templateSelection: formatStateSel,
        tokenSeparators: [',', ' ']
    });

    $('.select2-1-a').select2({
        width: '100%',
        placeholder: $('.select2-1-a').attr('placeholder'),
        tags: true,
        tokenSeparators: [',', ' '],
        closeOnSelect: false,
        templateResult: formatState,
        templateSelection: formatStateSel,
    });
    $('.select2-search__field').each(function() {
        $(this).attr('readonly', 'readonly');
    })

    $('.select2-1-a, .select2-1-b').on("change", function(e) {

        var wraper = $(this).attr('data-wraper');

        /* console.log(wraper);*/

        $('#' + wraper).find('.select2-selection').scrollTop(0);
        var width = $('#' + wraper).find('.select2-selection__rendered').width();
        /* console.log(width);*/
        $('#' + wraper).find('.select2-selection__rendered li.select2-selection__choice span.sel-format span').remove();
        $('#' + wraper).find('.select2-selection__rendered li.select2-selection__choice span.sel-format').append('<span>,</span>');
        $('#' + wraper).find('.select2-selection__rendered li.select2-selection__choice:last span.sel-format span').remove();
        var last_visible = false;
        var has_hidden = false;
        var all_width = 0;
        $('#' + wraper).find('.select2-selection__rendered li.select2-selection__choice').each(function() {

            if (all_width + 5 + $(this).width() < width) {
                last_visible = this;

                $(this).addClass('is_visible');
            } else {
                has_hidden = true;
                $(this).removeClass('is_visible');
            }
            all_width = all_width + $(this).width() + 5;
            // console.log(all_width);

        });
        if (last_visible) {
            $('#' + wraper).find('.select2-selection__rendered li.select2-search--inline').addClass('hide-input');
        } else {
            $('#' + wraper).find('.select2-selection__rendered li.select2-search--inline').removeClass('hide-input');
        }
        if (has_hidden) {
            $(last_visible).find('span.sel-format>span').remove();
            $(last_visible).find('span.sel-format').append('<span>...</span>');
            $('#' + wraper).find('.select2-selection').scrollTop(0);
        }
        $('span.select-show-selected ul').remove();
        $('span.select-show-selected').append($('#' + wraper).find('.select2-selection__rendered').clone());
    });




/*
    var jqxhr = $.getJSON("/tools/bazar/data/liste-villes.json", {
        format: "json",
        processResults: function(data) {
            var arrayvilles = $.map(data, function(obj) {
                obj.id = obj.id || obj.la + '_' + obj.lo; // replace pk with your identifier
                obj.text = obj.text || obj.n; // replace pk with your identifier

                return obj;
            });
            return arrayvilles;
        }
    }).done(function(data) {
        //console.log(data) ;
        var arrayvilles = $.map(data, function(obj) {
            obj.id = obj.id || obj.la + '#' + obj.lo; // replace pk with your identifier
            obj.text = obj.text || obj.n; // replace pk with your identifier

            return obj;
        });
        arrayvilles.sort(SortByVille);

        $('.js-data-ville').select2({
            width: '100%',
            closeOnSelect: false,
            placeholder: 'Sélectionner une localité',
            data: arrayvilles,
            templateSelection: formatState
        });
        $('.js-data-ville').on('select2:selecting', function(e) {
            $('.js-data-ville').val(null).trigger('change');
        });
    });


    $('.js-data-villeb').select2({
      ajax: {
    	url: '/tools/bazar/data/liste-villes.json',
    	dataType: 'json',
    	processResults: function (data) {
    	 var arrayvilles = $.map(data, function (obj) {
    		  obj.id = obj.id || obj.la+'#'+obj.lo; // replace pk with your identifier
    		  obj.text = obj.text || obj.n; // replace pk with your identifier

    		  return obj;
    		});
    	  return {
    		results: arrayvilles
    	  };
    	}
      }
    });*/

    function reset_select() {

        //console.log(arguments[0]);
        var select = $(arguments[0].target);
        select.val(null).trigger('change');
        select.select2("close");
        select.select2("open");
        return false;

    }
    $('.select2-1').on('a_inserted', function(e) {

        var sel_element = e;
        //console.log(sel_element ) ;
        $('body>.select2-container--default>span a.select-reset').bind('click', $.proxy(reset_select, null, sel_element));

        $('body>.select2-container--default>span a.select-close').bind('click', function() {
            $('.select2-1').select2('close');
            //console.log('clic') ;
        });

    });



    $('.select2-1').on('select2:open', function(e) {

        if ($('body').width() > 767) {

            var left_pos = ($('body').width() - $('body>.select2-container--default>.select2-dropdown').width()) / 2;
            $('body>.select2-container--default>.select2-dropdown').css('left', left_pos);

        } else {
            $('body>.select2-container--default>.select2-dropdown').css('left', 15);
        }

        $('body>.select2-container--default>.select2-dropdown .select2-results').height($('body>.select2-container--default>.select2-dropdown').height() - 60);
        if ($("body>.select2-container--default>span a").length < 1) {
            $('body>.select2-container--default>span').append('<span class="select-show-selected"></span>');
            $('body>.select2-container--default>span').append('<a href="#" class="select-close">Close</a>');

            var event = jQuery.Event("a_inserted");
            $(this).trigger(event);

        } else {

        }
        return false;
    });



    if ($('#search-carte-results').length > 0) {

        $('body').addClass('page-search-results-carte');

        $('#search-carte-results').css('height', ($(window).height() - $('#header-nav').height() - 8));

        $('#search-carte-results #map-buttons .legende-buttom, #map-legende .close-legende').bind('click', function() {
            toggle_legende_map();
        });
        $('#map-legende').swipe({
            swipe: function(event, direction, distance, duration, fingerCount) {

                if ((direction == 'left') && (distance > 30)) {
                    toggle_legende_map();

                }
            },
            allowPageScroll: "vertical"
        });



        function toggle_legende_map() {
            $('#map-legende').toggleClass('open');
            if ($('#map-legende').hasClass('open')) {
                $('body').addClass('menu-open');

            } else {
                $('body').removeClass('menu-open');
            }
            return false;
        }


        $(window).resize(function() {
            $('#search-carte-results').css('height', ($(window).height() - $('#header-nav').height()));
        });

        $('#infos_fiche_select .preview-nav-fiche .pull-left').bind('click', function() {
            show_fiche_full();
        });

        $('#infos_fiche_select .preview-nav-fiche .pull-right').bind('click', function() {
            hide_fiche();
        });

        $("#infos_fiche_select").swipe({
            swipe: function(event, direction, distance, duration, fingerCount) {

                if ((direction == 'up') && (distance > 30)) {
                    if (!$('#infos_fiche_select').hasClass('infos-full')) {
                        show_fiche_full();
                    }

                }
                if (direction == 'down') {
                    if (($('#infos_fiche_select').hasClass('infos-full')) && (distance > 100)) {
                        if ($('#infos_fiche_select .content-fiche').scrollTop() == 0) {
                            hide_fiche();
                        }

                    } else {
                        hide_fiche();
                    }
                    return false;
                }
            },
            allowPageScroll: "vertical",
			preventDefaultEvents: false
        });




    }

    function inti_menu() {
        if ($(window).width() < 768) {
            $('.dropdown-toggle').attr('data-toggle', '');


        } else {
            $('.dropdown-toggle').attr('data-toggle', 'dropdown');
            $('.navbar-collapse').removeClass('expended');
        }
    }
    inti_menu();
    $(window).resize(function() {
        inti_menu();
    });

    $('#header-nav .navbar-toggle, #close-nav-bar').bind('click', function() {
        $('#header-nav .navbar-collapse').toggleClass('expended');
        //console.log('hohoho') ;
        return false;
    });

    /*Retrive carousel auto on content and construct it */
    construct_carroussel();


    $(window).on('hashchange', function() {
        //console.log(window.location.hash) ;
        if (window.location.hash == '') {
            hide_fiche();
        } else {
            var id_fiche = window.location.hash.replace("#", "");;
            $('[data-id_fiche="' + id_fiche + '"]').parent('div').click();
        }
    });
    if (window.location.hash != '') {
        var id_fiche = window.location.hash.replace("#", "");;
        $('[data-id_fiche="' + id_fiche + '"]').parent('div:not(selectedMarker)').click();
        //console.log(window.location.hash) ;
    }


});
var prev_icon = false;

function show_fiche_marker(content, e) {

    var id_fiche = $(e.target._icon).find('.bazar-entry').attr('data-id_fiche');

    //console.log(e.target._icon) ;
    //console.log(id_fiche) ;
    window.location.hash = id_fiche;
    $('.leaflet-marker-pane>div').removeClass('selectedMarker');
    $(e.target._icon).addClass('selectedMarker');
    //$('#infos_fiche_select .content-fiche').animate({ scrollTop: 0 }, "500");
    $('#infos_fiche_select .content-fiche').html(content);
    $('#infos_fiche_select').addClass('infos-preview');
    $('.carousel').carousel();
    $('[data-toggle="tooltip"]').tooltip();
    $('.btn-close-full-fiche').bind('click', function() {
        hide_fiche();
        return false;
    });
    $('.btn-partage').bind('click', function(event) {
       event.preventDefault();
	   
	   /* if (navigator.share) {
            navigator.share({
                title: $(this).attr('data-fiche-title') + ' sur le site Atable',
                text: '',
                url: $(this).attr('data-link')
            }).then(function() { console.log('Successful share'); }).catch(function(err) {
                console.log('Couldn\'t share because of', err.message);
            });
        } else {
            console.log('No sharring mobile');
            return true;
        }*/
		event.preventDefault();
		
		$('#partager').show() ;
		$('#partager').find('.partage-overlay, .wrap-close>a').bind('click', function (event) {
			 event.preventDefault();
			$('#partager').hide() ;
			$('#partager').find('.partage-overlay, .wrap-close>a, .wrap-url>a').unbind('click') ;
			return false ;
		}) ;
		$('#partager').find('.wrap-url>a').bind('click', function (event) {
			 event.preventDefault();
			CopyToClipboard('partage-url-input') ;
			return false;
		}) ;
		
		function CopyToClipboard(containerid) {
			if (document.selection) { 
				var range = document.body.createTextRange();
				range.moveToElementText(document.getElementById(containerid));
				range.select().createTextRange();
				document.execCommand("copy"); 
				

			} else if (window.getSelection) {
				var range = document.createRange();
				 range.selectNode(document.getElementById(containerid));
				 window.getSelection().addRange(range);
				 document.execCommand("copy");
			}
		}
		

    });
	trigger_event_suppress() ;



}

function show_fiche_full() {

    $('#infos_fiche_select').addClass('infos-full');
}

function hide_fiche() {
    $('#infos_fiche_select').removeClass('infos-full');
    $('#infos_fiche_select').removeClass('infos-preview');
    window.location.hash = '';
}

function trigger_event_suppress() {
		jQuery('.btn-suppr-confirm').click(function(e) {
			var confirmation =  confirm('Etes vous sûr de vouloir supprimer la fiche ?');
			if (confirmation) {
				$(this).attr('href', $(this).attr('href') +'&confimation=true' ); 
				return true ;
			} else {
				return false ;
			}
		}) ;
}

//This will sort your array
function SortByVille(a, b) {
    //var aName = a.text.toLowerCase();
    //var bName = b.text.toLowerCase(); 
    //return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    return (a.text).localeCompare(b.text);
}
var nb_carousel = 0;

function construct_carroussel() {


    var content_html_canvas = '<div class="carousel slide" data-ride="carousel"><ol class="carousel-indicators"></ol><div class="carousel-inner" role="listbox"></div></div>';
    var li_canvas = '<li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>';
    var item_canvas = '<div class="item"></div>';

    if ($('.carousel-auto').length > 0) {

        $('.carousel-auto').each(function() {
            nb_carousel++;
            var id_carousel = 'carousel-js-' + nb_carousel;
            var img = $(this).find('img');
            var this_carousel = this;

            var content_carousel = $(content_html_canvas);


            if ($(img).length > 0) {
                var nb_img = 0;
                $(img).each(function() {

                    var li = $(li_canvas);
                    var item = $(item_canvas);
                    $(item).append(this);

                    $(li).attr('data-target', id_carousel);
                    $(li).attr('data-slide-to', nb_img);

                    if (nb_img == 0) {
                        $(li).addClass('active');
                        $(item).addClass('active');
                    }


                    $(content_carousel).find('ol').append($(li));
                    $(content_carousel).find('.carousel-inner').append($(item));
                    nb_img++;
                });


                $(content_carousel).attr('id', id_carousel);
                $(content_carousel).carousel();
                $(this_carousel).append($(content_carousel));



            }

        });

        $('.carousel').find('li').bind('click', function() {


            var id = $(this).attr('data-target');
            var num = parseInt($(this).attr('data-slide-to'));
            //console.log('hohoohoh-'+num+'-'+id) ;
            //console.log(num) ;
            $(('#' + id)).carousel(num);
        });

    }
}
function findBootstrapEnvironment() {
    var envs = ['xs', 'sm', 'md', 'lg'];

    var $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];

        $el.addClass('hidden-'+env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env;
        }
    }
}


! function(t) {
    var i = t(window);
    t.fn.visible = function(t, e, o) {
        if (!(this.length < 1)) {
            var r = this.length > 1 ? this.eq(0) : this,
                n = r.get(0),
                f = i.width(),
                h = i.height(),
                o = o ? o : "both",
                l = e === !0 ? n.offsetWidth * n.offsetHeight : !0;
            if ("function" == typeof n.getBoundingClientRect) {
                var g = n.getBoundingClientRect(),
                    u = g.top >= 0 && g.top < h,
                    s = g.bottom > 0 && g.bottom <= h,
                    c = g.left >= 0 && g.left < f,
                    a = g.right > 0 && g.right <= f,
                    v = t ? u || s : u && s,
                    b = t ? c || a : c && a;
                if ("both" === o) return l && v && b;
                if ("vertical" === o) return l && v;
                if ("horizontal" === o) return l && b
            } else {
                var d = i.scrollTop(),
                    p = d + h,
                    w = i.scrollLeft(),
                    m = w + f,
                    y = r.offset(),
                    z = y.top,
                    B = z + r.height(),
                    C = y.left,
                    R = C + r.width(),
                    j = t === !0 ? B : z,
                    q = t === !0 ? z : B,
                    H = t === !0 ? R : C,
                    L = t === !0 ? C : R;
                if ("both" === o) return !!l && p >= q && j >= d && m >= L && H >= w;
                if ("vertical" === o) return !!l && p >= q && j >= d;
                if ("horizontal" === o) return !!l && m >= L && H >= w
            }
        }
    }
}(jQuery);