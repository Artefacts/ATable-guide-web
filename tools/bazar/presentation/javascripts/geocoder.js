/**
 * Address search (geocoder).
 *
 * Recherche d'adresse (géocodage).
 */

/**
 * @param address The address to find.
 * @param codepostal could be null.
 * @param callbackOk( longitude, latitude ) The code to call when address found.
 * @param callbackError( error_message ) The code to call when address not found or an error occured.
 */
function geocodage( street, postcode, city, country, callbackOk, callbackError )
{
  var asyncCalls = new Array();
  var q ;

  //address = address.replace(/\\("|\'|\\)/g, " ").trim();

  // ask data.gouv.fr, result as GeoCodeJSON
  if( country && country.trim().length>0 && country.trim().toLowerCase() == 'france')
  {
    q = encodeURIComponent( street +' '+ city);
    if( postcode && postcode.trim().length > 0 ){
      q += '&postcode=' + encodeURIComponent(postcode.trim() );
    }
    asyncCalls.push(
      $.get('//api-adresse.data.gouv.fr/search/?q='+q)
    );
  }
  else
    asyncCalls.push(null);

  // ask nominatim, with the full address
  q = (street+' '+postcode+' '+city+' '+country).replace(/\\("|\'|\\)/g, " ").trim();
  asyncCalls.push(
    $.get('//nominatim.openstreetmap.org/search?q='+encodeURIComponent(q)+'&format=json')
  );

  // ask nominatim, without address street number
  q = (street+' '+postcode+' '+city +' '+country).replace(/\\("|\'|\\)/g, " ").trim();
  if( found = q.match( /^\d+(.*)/i ) )
  {
    q = found[1] ;
    asyncCalls.push(
      $.get('//nominatim.openstreetmap.org/search?q='+encodeURIComponent(q)+'&format=json')
    );
  }

  $.when.apply( $, asyncCalls )
  .then( function( dataGouv, nominatim1, nominatim2 )
  {
    if( dataGouv && dataGouv[0] && dataGouv[0].features && dataGouv[0].features.length > 0 )
    {
      //console.log('geocodage dataGouv');
      var c = dataGouv[0].features[0].geometry ;
      callbackOk( c.coordinates[0], c.coordinates[1] );
      return ;
    }

    if( nominatim1 && nominatim1[0] && nominatim1[0].length > 0 )
    {
      //console.log('geocodage nominatim1');
      callbackOk( nominatim1[0][0].lon, nominatim1[0][0].lat );
      return ;
    }

    if( nominatim2 && nominatim2[0] && nominatim2[0].length > 0 )
    {
      //console.log('geocodage nominatim2');
      callbackOk( nominatim2[0][0].lon, nominatim2[0][0].lat );
      return ;
    }

    callbackError('not found');

  });

};
