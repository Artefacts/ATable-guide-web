<?php

/**
 *  Programme gerant les fiches bazar depuis une interface de type geographique.
 **/

// +------------------------------------------------------------------------------------------------------+
// |                                            ENTETE du PROGRAMME                                       |
// +------------------------------------------------------------------------------------------------------+

function bazAt_requete_recherche_fiches(
    $tableau_criteres = '',
    $tri = '',
    $id = '',
    $categorie_fiche = '',
    $statut = 1,
    $personne = '',
    $nb_limite = '',
    $motcles = true,
    $q = '',
    $facettesearch = 'OR'
) {
  // error_log(__FUNCTION__);

    //si les parametres ne sont pas rentres, on prend les variables globales
    if ($categorie_fiche == '' &&
        !empty($GLOBALS['params']['categorienature'])) {
        $categorie_fiche = $GLOBALS['params']['categorienature'];
    }

    //requete pour recuperer toutes les PageWiki etant des fiches bazar
    $requete_pages_wiki_bazar_fiches =
    'SELECT DISTINCT resource FROM '.$GLOBALS['wiki']->config['table_prefix'].'triples '.
    'WHERE value = "fiche_bazar" AND property = "http://outils-reseaux.org/_vocabulary/type" '.
    'ORDER BY resource ASC';

    $requete =
    'SELECT DISTINCT * FROM '.$GLOBALS['wiki']->config['table_prefix'].
    'pages WHERE latest="Y"  AND body LIKE \'%"radioListeValidation":"2"%\' AND body LIKE \'%"radioListeSuppression":"1"%\'  AND comment_on = \'\'';

    //on limite au type de fiche
    if (!empty($id)) {
        if (is_array($id)) {
            if (count($id) == 1) {
                // on a qu'un id dans le tableau
                $id = array_shift($id);
                $requete .= ' AND body LIKE \'%"id_typeannonce":"'.$id.'"%\'';
            } else {
                // on a plusieurs id dans le tableau
                $requete .= ' AND ';
                $first = true;
                foreach ($id as $formid) {
                    if ($first) {
                        $first = false;
                    } else {
                        $requete .= ' OR ';
                    }
                    $requete .= 'body LIKE \'%"id_typeannonce":"'.$formid.'"%\'';
                }
            }
        } else {
            // on a une chaine de caractere pour l'id plutot qu'un tableau
            $requete .= ' AND body LIKE \'%"id_typeannonce":"'.$id.'"%\'';
        }
    }

    //statut de validation
    $requete .= ' AND body LIKE \'%"statut_fiche":"'.$statut.'"%\'';

    // periode de modification
    if (!empty($GLOBALS['params']['datemin'])) {
        $requete .= ' AND time >= "'.$GLOBALS['params']['datemin'].'"';
    }

    //si une personne a ete precisee, on limite la recherche sur elle
    if ($personne != '') {
        $requete .=
        ' AND body LIKE \'%"createur":"'.utf8_encode($personne).'"%\'';
    }

    $requete .= ' AND tag IN ('.$requete_pages_wiki_bazar_fiches.')';

    $requeteSQL = '';
    //preparation de la requete pour trouver les mots cles
    if (trim($q) != '' && $q !=_t('BAZ_MOT_CLE')) {
        $GLOBALS['wiki']->Query("SET sql_mode = 'NO_BACKSLASH_ESCAPES';");
        $search = str_replace(array('["', '"]'), '', json_encode(array(removeAccents($q))));
        $recherche = explode(' ', $search);
		//var_dump($recheche) ;
        $nbmots = count($recherche);
        $requeteSQL .= ' AND (';
        for ($i = 0; $i < $nbmots; ++$i) {
            if ($i > 0) {
                $requeteSQL .= ' OR ';
            }

            //$requeteSQL .= ' body LIKE \'%'. mysqli_escape_string($GLOBALS['wiki']->dblink, $recherche[$i]).'%\'';
			
			$requeteSQL .= "match(tag, body) against('" . mysqli_real_escape_string($GLOBALS['wiki']->dblink, $recherche[$i]) . "')";
        }
        $requeteSQL .= ')';
    }
	//var_dump($requeteSQL) ;
    //on ajoute dans la requete les valeurs passees dans les champs liste et checkbox du moteur de recherche
    if ($tableau_criteres == '') {
        $tableau_criteres = array();

        // on transforme les specifications de recherche sur les liste et checkbox
        if (isset($_REQUEST['rechercher'])) {
            reset($_REQUEST);
            while (list($nom, $val) = each($_REQUEST)) {
                if (((substr($nom, 0, 5) == 'liste') || (substr($nom, 0, 8) ==
                    'checkbox')) && $val != '0' && $val != '') {
                    if (is_array($val)) {
                        $val = implode(',', array_keys($val));
                    }
                    $tableau_criteres[$nom] = $val;
                }
            }
        }
    }

    // cas des criteres passés en parametres get
    if (isset($_GET['query'])) {
        $query = $_GET['query'];
        $tableau = array();
        $tab = explode('|', $query);
        //découpe la requete autour des |
        foreach ($tab as $req) {
            $tabdecoup = explode('=', $req, 2);
            if (count($tabdecoup)>1) {
                $tableau[$tabdecoup[0]] = trim($tabdecoup[1]);
            }
        }
        $tableau_criteres = array_merge($tableau_criteres, $tableau);
    }

    if ($motcles == true) {
        reset($tableau_criteres);

        while (list($nom, $val) = each($tableau_criteres)) {
            if (!empty($nom) && !empty($val)) {
                $valcrit = explode(',', $val);
                if (is_array($valcrit) && count($valcrit) > 1) {
                    $requeteSQL .= ' AND (';
                    $first = true;
                    foreach ($valcrit as $critere) {
                        if (!$first) {
                            $requeteSQL .= ' '.$facettesearch.' ';
                        }
                        $requeteSQL .=
                        'body LIKE \'%"'.$nom.'":"'.$critere.'"%\' or '
                        .'body LIKE \'%"'.$nom.'":"'.$critere.',%\'or '
                        .'body LIKE \'%"'.$nom.'":"%,'.$critere.'"%\' or '
                        .'body LIKE \'%"'.$nom.'":"%,'.$critere.',%\'';

                        $first = false;
                    }
                    $requeteSQL .= ')';
                } else {
                    if (strcmp(substr($nom, 0, 5), 'liste') == 0) {
                        $requeteSQL .=
                        ' AND (body LIKE \'%"'.$nom.'":"'.$val.'"%\')';
                    } else {
                        $requeteSQL .=
                        ' AND (body LIKE \'%"'.$nom.'":"'.$val.'"%\' or '
                        .'body LIKE \'%"'.$nom.'":"'.$val.',%\' or '
                        .'body LIKE \'%"'.$nom.'":"%,'.$val.'"%\' or '
                        .'body LIKE \'%"'.$nom.'":"%,'.$val.',%\')';
                    }
                }
            }
        }
    }

    // requete de jointure : reprend la requete precedente et ajoute des criteres
    if (isset($_GET['joinquery'])) {
        $joinrequeteSQL = '';
        $tableau = array();
        $tab = explode('|', $_GET['joinquery']);
        //découpe la requete autour des |
        foreach ($tab as $req) {
            $tabdecoup = explode('=', $req, 2);
            $tableau[$tabdecoup[0]] = trim($tabdecoup[1]);
        }
        $first = true;
        while (list($nom, $val) = each($tableau)) {
            if (!empty($nom) && !empty($val)) {
                $valcrit = explode(',', $val);
                if (is_array($valcrit) && count($valcrit) > 1) {
                    foreach ($valcrit as $critere) {
                        if (!$first) {
                            $joinrequeteSQL .= ' AND ';
                        } else {
                            $first = false;
                        }
                        $joinrequeteSQL .=
                        '(body REGEXP \'"'.$nom.'":"[^"]*'.$critere.
                        '[^"]*"\')';
                    }
                    $joinrequeteSQL .= ')';
                } else {
                    if (!$first) {
                        $joinrequeteSQL .= ' AND ';
                    } else {
                        $first = false;
                    }
                    if (strcmp(substr($nom, 0, 5), 'liste') == 0) {
                        $joinrequeteSQL .=
                        '(body REGEXP \'"'.$nom.'":"'.$val.'"\')';
                    } else {
                        $joinrequeteSQL .=
                        '(body REGEXP \'"'.$nom.'":("'.$val.
                        '"|"[^"]*,'.$val.'"|"'.$val.',[^"]*"|"[^"]*,'
                        .$val.',[^"]*")\')';
                    }
                }
            }
        }
        if ($requeteSQL != '') {
            $requeteSQL .= ' UNION
            '.$requete.' AND ('.$joinrequeteSQL.')';
        } else {
            $requeteSQL .= ' AND ('.$joinrequeteSQL.')';
        }
        $requete .= $requeteSQL;
    } elseif ($requeteSQL != '') {
        $requete .= $requeteSQL;
    }
	//var_dump($requete); 
//error_log($requete);
    // systeme de cache des recherches
    $reqid = 'bazar-search-'.md5($requete);
    if (!isset($GLOBALS['_BAZAR_'][$reqid])) {
        // debug
        if (isset($_GET['showreq'])) {
            echo '<hr><code style="width:100%;height:100px;">'.$requete.'</code><hr>';
        }
        $GLOBALS['_BAZAR_'][$reqid] = $GLOBALS['wiki']->LoadAll($requete);
    }
//error_log( 'Results: '. count($GLOBALS['_BAZAR_'][$reqid]) );
    return $GLOBALS['_BAZAR_'][$reqid];
}


// test de sécurité pour vérifier si on passe par wiki
if (!defined('WIKINI_VERSION')) {
    die('acc&egrave;s direct interdit');
}
$inputs = array('listeListeTypeStructure', 'checkboxListeProduits') ;
foreach ($inputs as $name) {

	if (isset($_REQUEST[$name])) {
		$temp = $_REQUEST[$name] ;
		unset($_REQUEST[$name]) ;
		foreach ($temp as $value) {
			$_REQUEST[$name][$value] = $value ;
		}
	}

}

// Recuperation de tous les parametres
$GLOBALS['params'] = getAllParameters($this);

$q = false;
if (isset($_GET['q'])) {
		$q= filter_var($_GET['q'], FILTER_SANITIZE_STRING) ;
		$q= filter_var($q, FILTER_SANITIZE_MAGIC_QUOTES) ;
        //var_dump($q) ;
    }

// tableau des fiches correspondantes aux critères
if (is_array($GLOBALS['params']['idtypeannonce'])) {
    $results = array();
    foreach ($GLOBALS['params']['idtypeannonce'] as $formid) {
        $results = array_merge(
            $results,
            bazAt_requete_recherche_fiches($GLOBALS['params']['query'], 'alphabetique', $formid, '', 1, '', '', true, $q)
        );
    }
} else {
    $results = bazAt_requete_recherche_fiches($GLOBALS['params']['query'], 'alphabetique', '', '', 1, '', '', true, $q);
}

// a la place du choix par défaut, on affiche en carte
if ($GLOBALS['params']['template'] == $GLOBALS['wiki']->config['default_bazar_template']) {
    $GLOBALS['params']['template'] = 'map.tpl.html';
}

// affichage à l'écran
echo displayResultList($results, $GLOBALS['params'], false);
