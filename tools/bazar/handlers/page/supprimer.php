<?php
/*
$Id: __show.php,v 1.1 2010-06-02 08:48:51 mrflos Exp $
Copyright (c) 2010, Florian Schmitt <florian@outils-reseaux.org>
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Vérification de sécurité
if (!defined("WIKINI_VERSION")) {
    die("acc&egrave;s direct interdit");
}
global $LABELS_SITE ;

$is_admin = false ;
	$save = true ;
	foreach ($GLOBALS['wiki']-> GetGroupsList() as $key => $val) {
		if ($GLOBALS['wiki']->UserIsInGroup($val) ) {
			$is_admin = true;
		} 
    }
	
$tag = $this->GetPageTag();
	
//echo $this->GetPageTag() ;

echo  $this->Header();
//si la page est de type fiche_bazar, alors on affiche la fiche plutot que de formater en wiki
$type = $this->GetTripleValue($this->GetPageTag(), 'http://outils-reseaux.org/_vocabulary/type', '', '');
if ($type == 'fiche_bazar') {
    $tab_valeurs = baz_valeurs_fiche($this->GetPageTag());
    $this->page["body"] = '""'.baz_voir_fiche(0, $tab_valeurs).'""';
	if ($is_admin) {
		
		if (!$_GET['confirme'] == 'oui')
		{			
			$msg = '<form action="'.$this->Href('deletepage', "", "confirme=oui");
			$msg .= '" method="post" style="display: inline">'."\n";
			$msg .= 'Voulez-vous vraiment supprimer d&eacute;finitivement la page '.$this->Link($tag)."&nbsp;?\n";
			$msg .= '<input type="submit" value="Oui" ';
			$msg .= 'style="vertical-align: middle; display: inline" />'."\n";
			$msg .= "</form>\n";			
			$msg .= '<form action="'.$this->Href().'" method="post" style="display: inline">'."\n";
			$msg .= '<input type="submit" name="deleteexit" value="Non" style="vertical-align: middle; display: inline" />'."\n";
			$msg .= "</form></span>\n";		

			echo $msg ;			
		} else {
			baz_suppression($this->GetPageTag()) ;
		}
	} else {
		//var_dump($tab_valeurs) ;
		if (isset($_GET['confimation'])) {
			
			baz_evoi_mail($LABELS_SITE['ObjMail_SuppFicheDemande'].' : '.$tag, $tab_valeurs ) ;
			echo
            '<div class="alert alert-error alert-info">'.$LABELS_SITE['SuppFiche_MessDemande'].'</div>'."\n";
		} else {
			echo
            '<div class="alert alert-error alert-danger">La demande de suppression n\'a pas pu être envoyée !</div>'."\n";
		}
		
	}
            //die($plugin_output_new);
}

echo $this->Footer();

die() ;
