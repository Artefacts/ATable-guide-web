<?php
if (!defined("WIKINI_VERSION"))
{
        die ("acc&egrave;s direct interdit");
}


global $LABELS_SITE ;

// Liste des clés de labels utilisé par du javascript ;
$all_labels = array('FormFiche_MessChampOblVide', 'FormFiche_MessChampGeoloc') ;


foreach ($all_labels as $label) {

	if (isset ($LABELS_SITE[$label])) {
		echo '<div data-label-key="'.$label.'" class="hidden">'.$LABELS_SITE[$label].'</div>' ;
	}
}